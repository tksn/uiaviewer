from PIL import Image


_DEFAULT_RESTRICTED = (20, 80)
_DEFAULT_UNRESTRICTED = (20, 100)
_DEFAULT_SCALED = (10, 50)


def install(monkeypatch):
    dummy_dev = Device()

    def CreateDevice(progress):
        return dummy_dev

    monkeypatch.setattr('uiaviewer.control.Device', CreateDevice)
    return dummy_dev


class Device(object):
    """Android device"""

    def __init__(self, serialno=None, progress=None):
        self.__server_activated = False
        self.__window_size = {'restricted': _DEFAULT_RESTRICTED, 'unrestricted': _DEFAULT_UNRESTRICTED}
        self.__hierarchy_dump = b'<root><node text="abc"></node></root>'

    def __ensure_uiautomator_server_inactive(self, progress):
        pass

    def activate_uiautomator_server(self):
        self.__server_activated = True

    def deactivate_uiautomator_server(self):
        self.__server_activated = False

    @property
    def uiautomator_server_activated(self):
        return self.__server_activated

    def screencap(self):
        return Image.new('RGB', self.get_window_size()['unrestricted'])

    def hierarchy_dump(self):
        return self.__hierarchy_dump

    def get_window_size(self):
        return self.__window_size

    def set_window_size(self, restricted, unrestricted):
        self.__window_size = {'restricted': restricted, 'unrestricted': unrestricted}

    @property
    def uiautomator_device(self):
        return 'dummy uiautomator'

    def set_hierarchy_dump(self, dump):
        self.__hierarchy_dump = dump
