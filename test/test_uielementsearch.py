from __future__ import unicode_literals
import pytest
import uiaviewer.uielementsearch
import uiaviewer.uihierarchy


def create_bounds_str(left, top, right, bottom):
    return '[{0},{1}][{2},{3}]'.format(left, top, right, bottom)


def create_two(l0, t0, r0, b0, l1, t1, r1, b1):
    a = create_bounds_str(l0, t0, r0, b0)
    b = create_bounds_str(l1, t1, r1, b1)
    return uiaviewer.uihierarchy.UiHierarchy.frombytes(
        '<node bounds="{0}"><node bounds="{1}"></node></node>'.format(a, b).encode('utf-8'))


def create_one(left, top, right, bottom):
    return uiaviewer.uihierarchy.UiHierarchy.frombytes(
        '<node bounds="{0}"></node>'.format(create_bounds_str(left, top, right, bottom)).encode('utf-8'))


def bounds(left, top, right, bottom):
    return {'left': left, 'top': top, 'right': right, 'bottom':bottom}


def test_smallest_containing_one():
    root = create_one(0, 0, 10, 10)
    smallest = uiaviewer.uielementsearch.get_smallest_containing(root, (5, 5))
    assert smallest is root
    smallest = uiaviewer.uielementsearch.get_smallest_containing(root, (0, 0))
    assert smallest is root
    smallest = uiaviewer.uielementsearch.get_smallest_containing(root, (9, 9))
    assert smallest is root


def test_smallest_containing_one_just_outside():
    root = create_one(0, 0, 10, 10)
    smallest = uiaviewer.uielementsearch.get_smallest_containing(root, (0, 10))
    assert smallest is None
    smallest = uiaviewer.uielementsearch.get_smallest_containing(root, (10, 0))
    assert smallest is None


def test_smallest_containing_two_inner():
    root = create_two(0, 0, 20, 20, 5, 5, 15, 15)
    smallest = uiaviewer.uielementsearch.get_smallest_containing(root, (10, 10))
    assert smallest.bounds == bounds(5, 5, 15, 15)


def test_smallest_containing_two_outer():
    root = create_two(0, 0, 20, 20, 5, 5, 15, 15)
    smallest = uiaviewer.uielementsearch.get_smallest_containing(root, (3, 3))
    assert smallest.bounds == bounds(0, 0, 20, 20)


def test_smallest_containing_double():
    a = create_bounds_str(0, 0, 20, 20)
    b = create_bounds_str(8, 8, 15, 15)
    c = create_bounds_str(5, 5, 15, 15)
    root = uiaviewer.uihierarchy.UiHierarchy.frombytes(
        ''.join((
            '<node bounds="{0}">',
            '<node bounds="{1}"></node>',
            '<node bounds="{2}"></node>',
            '</node>'
        )).format(a, b, c).encode('utf-8'))
    smallest = uiaviewer.uielementsearch.get_smallest_containing(root, (10, 10))
    assert smallest.bounds == bounds(8, 8, 15, 15)


def test_generate_locator_expression():
    expr = uiaviewer.uielementsearch.generate_locator_expression((
        ('checkable', 'true'),
        ('class', 'abc.def'),
        ('index', '1')))
    assert expr == 'checkable=True, className=\'abc.def\', index=1'


def check_locatable(attr_str, locator_str, count=2):
    root = uiaviewer.uihierarchy.UiHierarchy.frombytes(
        '<node><node></node><node {0}></node><node {0}></node></node>'.format(attr_str).encode('utf-8'))
    c = uiaviewer.uielementsearch.quicktest_locator_expression(root, locator_str)
    assert c == count


def check_not_locatable(attr_str, locator_str):
    check_locatable(attr_str, locator_str, 0)


def test_quicktest_locator_expression_text_equals():
    check_locatable('text="abc"', 'text=\'abc\'')


def test_quicktest_locator_expression_bool_equals():
    check_locatable('clickable="true"', 'clickable=True')


def test_quicktest_locator_expression_int_equals():
    check_locatable('index="3"', 'index=3')


def test_quicktest_locator_expression_multi_condition():
    root = uiaviewer.uihierarchy.UiHierarchy.frombytes((
        '<node>'
        '<node text="abc"></node>'
        '<node text="abc" clickable="true"></node>'
        '<node clickable="true"></node>'
        '</node>').encode('utf-8'))
    assert 2 == uiaviewer.uielementsearch.quicktest_locator_expression(root, 'text=\'abc\'')
    assert 2 == uiaviewer.uielementsearch.quicktest_locator_expression(root, 'clickable=True')
    assert 1 == uiaviewer.uielementsearch.quicktest_locator_expression(root, 'text=\'abc\', clickable=True')


def test_quicktest_locator_expression_text_contains():
    check_locatable('text="89abcde"', 'textContains=\'abc\'')
    check_not_locatable('text="89abbcde"', 'textContains=\'abc\'')


def test_quicktest_locator_expression_text_startswith():
    check_locatable('text="abcde"', 'textStartsWith=\'abc\'')
    check_not_locatable('text="89abcde"', 'textStartsWith=\'abc\'')


def test_quicktest_locator_expression_text_match():
    check_locatable('text="abcde"', 'textMatches=\'^ab.*\'')
    check_not_locatable('text="a-b-c-d-e"', 'textMatches=\'^ac.*\'')


def test_quicktest_locator_expression_invalid():
    with pytest.raises(ValueError):
        check_locatable('text="89abcde"', 'textIncludes=\'abc\'')
    with pytest.raises(ValueError):
        check_locatable('text="89abcde"', 'text')
    with pytest.raises(ValueError):
        check_locatable('text="89abcde"', 'text=ABC')
