import os
from uiaviewer.uiaview import run_viewer

def load_window_dump_xml():
    xmlpath = os.path.join(os.path.dirname(__file__), 'window_dump.xml')
    with open(xmlpath, 'rb') as f:
        return f.read()

DUMP_XML = load_window_dump_xml()
DEVICE_SCREEN_SIZES = ((1440, 2392), (1440, 2562))

PLAY_TEXTVIEW_COORD = (337, 616)

def exec_main(mocks, test_func):

    def in_mainloop(root):
        root.call_after_func()
        test_func(root)

    mocks.tkroot.set_mainloop_testfunc(in_mainloop)
    run_viewer(argv=['-w', '480x800'])


def test_hierarchy_view_display(mocks):
    mocks.device.set_hierarchy_dump(DUMP_XML)
    mocks.device.set_window_size(*DEVICE_SCREEN_SIZES)

    def test(root):
        path_to_play = (0, 0, 0, 0, 0, 0, 0, 0, 2)
        play_text = root.descendant('hviewtreeview').get_tree_node_text(*path_to_play)
        assert 'Play' in play_text

    exec_main(mocks, test)


def test_attributes_view_display(mocks):
    mocks.device.set_hierarchy_dump(DUMP_XML)
    mocks.device.set_window_size(*DEVICE_SCREEN_SIZES)

    def test(root):
        path_to_play = (0, 0, 0, 0, 0, 0, 0, 0, 2)
        hview = root.descendant('hviewtreeview')
        hview.selection_set_by_tree_indexes(*path_to_play)
        hview.issue('<<TreeviewSelect>>', None)
        attrlist = root.descendant('attrlistbox')
        assert any('content-desc: Folder: Play' == item for item in attrlist.items)

    exec_main(mocks, test)


def test_attributes_view_select(mocks):
    mocks.device.set_hierarchy_dump(DUMP_XML)
    mocks.device.set_window_size(*DEVICE_SCREEN_SIZES)

    def test(root):
        path_to_play = (0, 0, 0, 0, 0, 0, 0, 0, 2)

        # Click "Folder: Play" in hierarchy view treeview
        hview = root.descendant('hviewtreeview')
        hview.selection_set_by_tree_indexes(*path_to_play)
        hview.issue('<<TreeviewSelect>>', None)

        # Click an item begins with 'content-desc' in attribute view listbox
        attrlist = root.descendant('attrlistbox')
        attrlist.select('content-desc')
        attrlist.call_after_func()

        # Check generated location expression
        locexpr = root.descendant('locexprtext')
        expr = locexpr.get()
        assert 'description=\'Folder: Play\'' in expr

        # Manually modify location expression
        locexpr.delete()
        editted_text = expr +  ', clickable=True'
        locexpr.insert(None, editted_text)
        locexpr.call_after_func()

        # Check generated match count string
        matchcount = root.descendant('matchcount')
        assert '1' in matchcount.config_items.get('text')

        # Click copy button
        locexprcopy = root.descendant('locexprcopy')
        locexprcopy.issue(None)
        assert locexpr.clipboard == editted_text

    exec_main(mocks, test)


def test_screen_click(mocks):
    mocks.device.set_hierarchy_dump(DUMP_XML)
    mocks.device.set_window_size(*DEVICE_SCREEN_SIZES)

    def test(root):
        class DummyEvent(object): x, y = PLAY_TEXTVIEW_COORD

        canvas = root.descendant('canvas')
        canvas.issue('<Button-1>', DummyEvent)

        hview = root.descendant('hviewtreeview')
        assert 'Play' in hview.iid_to_text(hview.selection()[0])

    exec_main(mocks, test)


def test_update(mocks):
    mocks.device.set_hierarchy_dump(DUMP_XML)
    mocks.device.set_window_size(*DEVICE_SCREEN_SIZES)

    def test(root):
        update = root.descendant('update')
        update.issue(None)
        hview = root.descendant('hviewtreeview')
        assert not hview.selection()

    exec_main(mocks, test)


def test_activate_uiautomator(mocks):
    mocks.device.set_hierarchy_dump(DUMP_XML)
    mocks.device.set_window_size(*DEVICE_SCREEN_SIZES)

    def test(root):
        activ = root.descendant('activateuiautomator')
        activ.issue(None)
        assert 'Deactivate' in activ.config_items['text']
        activ.issue(None)
        assert 'Activate' in activ.config_items['text']

    exec_main(mocks, test)
