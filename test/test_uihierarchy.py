from __future__ import unicode_literals
import pytest
import uiaviewer.uihierarchy


def test_uielement_str_expr_empty():
    elem = uiaviewer.uihierarchy.UiElement('node', {})
    assert str(elem) == '()'


def test_uielement_str_expr():
    elem = uiaviewer.uihierarchy.UiElement(
        'node', {
            'index': '0',
            'class': 'abc',
            'text': 'def',
            'content-desc': 'ghi',
            'bounds': '[123,45][67,890]'})
    assert str(elem) == '(0) abc "def" {ghi} [123,45][67,890]'


def test_uielement_bounds():
    elem = uiaviewer.uihierarchy.UiElement(
        'node', {'bounds': '[123,45][67,890]'})
    assert elem.bounds == {
        'top': 45, 'left': 123, 'bottom': 890, 'right': 67
    }


def test_uielement_corrupted_bounds():
    elem = uiaviewer.uihierarchy.UiElement(
        'node', {'bounds': '[123][67,890]'})
    with pytest.raises(ValueError):
        assert elem.bounds is not None


def test_uihierarchy_fromstring():
    s = b'<root></root>'
    root = uiaviewer.uihierarchy.UiHierarchy.frombytes(s)
    assert root.tag == 'root'


def test_uihierarchy_iterate():
    s = b'<root><node text="abc"></node><node text="def"></node></root>'
    root = uiaviewer.uihierarchy.UiHierarchy.frombytes(s)
    iter = root.get_uielement_iterator()
    assert next(iter).get('text') == 'abc'
    assert next(iter).get('text') == 'def'
    with pytest.raises(StopIteration):
        next(iter)
