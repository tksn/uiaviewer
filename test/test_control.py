from __future__ import unicode_literals
import mock
import uiaviewer.control

_DEFAULT_RESTRICTED = (20, 80)
_DEFAULT_UNRESTRICTED = (20, 100)
_DEFAULT_SCALED = (10, 50)


def create_screen_geometry(
        restricted=_DEFAULT_RESTRICTED,
        unrestricted=_DEFAULT_UNRESTRICTED,
        scaled=_DEFAULT_SCALED):
    def create_dummy_device():
        def window_size():
            return {
                'restricted': restricted,
                'unrestricted': unrestricted
            }
        class _Obj(object): pass
        o = _Obj()
        o.get_window_size = window_size
        return o
    geom = uiaviewer.control.ScreenGeometry(create_dummy_device(), scaled)
    return geom


def test_screen_size():
    geom = create_screen_geometry()
    assert geom.scaled_screen_size == (10, 50)
    assert geom.device_restricted_screen_size == (20, 80)
    assert geom.device_unrestricted_screen_size == (20, 100)


def test_screen_geometry_scale():
    geom = create_screen_geometry()
    assert geom.scale((10, 40)) == (5, 20)


def test_screen_geometry_descale():
    geom = create_screen_geometry()
    assert geom.descale((5, 20)) == (10, 40)


def test_screen_geometry_clip():
    geom = create_screen_geometry()

    before = (15, 85) # x:inside, y:outside
    after = geom.clip(before)
    assert before[0] == after[0]
    assert _DEFAULT_RESTRICTED[1] == after[1]

    before = (25, 70)  # x:outside, y:inside
    after = geom.clip(before)
    assert before[1] == after[1]
    assert _DEFAULT_RESTRICTED[0] == after[0]


def open_control_object(monkeypatch):

    m = mock.Mock()
    m.get_window_size.return_value = {'restricted': _DEFAULT_RESTRICTED, 'unrestricted': _DEFAULT_UNRESTRICTED}
    m.screencap.return_value = 'dummy screenshot'
    m.hierarchy_dump.return_value = b'<root><node text="abc"></node></root>'

    def dummy_progress(text):
        pass

    monkeypatch.setattr('uiaviewer.control.Device', mock.Mock(return_value=m))
    control = uiaviewer.control.Control()
    control.open(dummy_progress)
    return control, m


def test_control_create(monkeypatch):
    control, _ = open_control_object(monkeypatch)
    assert control.get_screenshot() == 'dummy screenshot'
    assert control.get_hierarchy_snapshot().findall('./node')[0].get('text') == 'abc'
    assert control.geometry.device_restricted_screen_size == _DEFAULT_RESTRICTED


def test_control_signaling(monkeypatch):
    control, _ = open_control_object(monkeypatch)

    result = []
    def listener(arg0, arg1):
        result.append(arg0)
        result.append(arg1)

    control.add_listener('sig0', listener)

    control.signal('sig0', 'abc', arg1='def')
    assert result == ['abc', 'def']


def test_control_uiautomator_control(monkeypatch):
    control, device = open_control_object(monkeypatch)
    device.uiautomator_server_activated = False
    assert control.uiautomator_activated == False
    device.uiautomator_server_activated = True
    assert control.uiautomator_activated == True

    control.activate_uiautomator()
    device.uiautomator_server_activated = False
    control.activate_uiautomator()
    device.activate_uiautomator_server.assert_called_once_with()
    device.uiautomator_server_activated = False
    control.deactivate_uiautomator()
    device.uiautomator_server_activated = True
    control.deactivate_uiautomator()
    device.deactivate_uiautomator_server.assert_called_once_with()

    device.uiautomator_device = 'abc'
    assert control.uiautomator_device == 'abc'



