from __future__ import unicode_literals
from mock import Mock


def install(monkeypatch):
    monkeypatch.setattr('threading.Thread', Thread)


class Thread(object):

    def __init__(self, target, args):
        self.__target = target
        self.__args = args

    def start(self):
        self.__target(*self.__args)
