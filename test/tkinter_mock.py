from __future__ import unicode_literals
from mock import Mock
import uuid


def install(monkeypatch):
    root = Tk()
    monkeypatch.setattr('tkinter.Tk', lambda: root)
    monkeypatch.setattr('tkinter.Toplevel', Widget)
    monkeypatch.setattr('tkinter.Canvas', Canvas)
    monkeypatch.setattr('tkinter.ttk.Frame', Widget)
    monkeypatch.setattr('tkinter.ttk.Button', Button)
    monkeypatch.setattr('tkinter.ttk.Entry', Entry)
    monkeypatch.setattr('tkinter.ttk.Label', Widget)
    monkeypatch.setattr('tkinter.Text', Text)
    monkeypatch.setattr('PIL.ImageTk.PhotoImage', Mock())

    monkeypatch.setattr('tkinter.ttk.Style', Mock())
    monkeypatch.setattr('tkinter.ttk.Treeview', Treeview)
    monkeypatch.setattr('tkinter.ttk.Scrollbar', Mock())
    monkeypatch.setattr('tkinter.Listbox', Listbox)
    monkeypatch.setattr('tkinter.ttk.Labelframe', Widget)

    return root


class Widget(object):

    def __init__(self, *args, **options):
        self.__master = None
        if len(args) > 0:
            master = args[0]
            master.add_child(options.get('name'), self)
            self.__master = master
        self.children = {}
        self.callbacks = []
        self.after_func = None

        mock_methods = ['grid', 'pack', 'destroy', 'update', 'pack_propagate',
                        'columnconfigure']
        for m in mock_methods:
            setattr(self, m, Mock())

        if options:
            self.config(**options)

        self.winfo_toplevel = Mock()

    @property
    def master(self):
        return self.__master

    def add_child(self, name, child_inst):
        name = name or uuid.uuid4()
        self.children[name] = child_inst

    def bind(self, event_name, callback):
        self.callbacks.append((event_name, callback))

    def config(self, *args, **kwargs):
        command_func = kwargs.get('command')
        if command_func:
            self.bind(None, command_func)
        self.config_items = dict(kwargs)

    def issue(self, event_name, *args, **kwargs):
        for evt, cb in self.callbacks:
            if event_name is None or evt == event_name:
                cb(*args, **kwargs)

    def nametowidget(self, name):
        descs = name.split('.')
        childname = descs[0]
        desc_inst = self.children[childname]
        if descs[1:]:
            desc_inst = desc_inst.nametowidget('.'.join(descs[1:]))
        return desc_inst

    def descendant(self, name):
        child = self.children.get(name)
        if child:
            return child
        for child in self.children.values():
            found = child.descendant(name)
            if found:
                return found
        return None

    def after(self, ms, after_func, *args):
        def after_func_wrap():
            after_func(*args)
        self.after_func = after_func_wrap

    def after_cancel(self, timer_id):
        self.after_func = None

    def call_after_func(self):
        if self.after_func is not None:
            self.after_func()


class Tk(Widget):

    def __init__(self):
        Widget.__init__(self)
        self.title = Mock()
        self.wait_window = Mock()
        self.__test = None

    def mainloop(self):
        self.__test(self)

    def set_mainloop_testfunc(self, testfunc):
        self.__test = testfunc


class Canvas(Widget):

    def __init__(self, *args, **kwargs):
        Widget.__init__(self, *args, **kwargs)
        self.delete = Mock()
        self.create_image = Mock()
        self.create_rectangle = Mock()
        self.wait_window = Mock()


class Button(Widget):

    def __init__(self, *args, **kwargs):
        Widget.__init__(self, *args, **kwargs)


class Entry(Widget):

    def __init__(self, *args, **kwargs):
        Widget.__init__(self, *args, **kwargs)
        self.get = Mock()
        self.delete = Mock()
        self.insert = Mock()


class Text(Widget):

    def __init__(self, *args, **kwargs):
        Widget.__init__(self, *args, **kwargs)
        self.see = Mock()
        self.clipboard = ''
        self.clipboard_clear = Mock()
        self.__text = ''

    def insert(self, pos, text):
        self.__text = text

    def delete(self, *args):
        self.__text = ''

    def get(self, *args):
        return self.__text

    def clipboard_append(self, text):
        self.clipboard = text


class Treeview(Widget):

    def __init__(self, *args, **kwargs):
        Widget.__init__(self, *args, **kwargs)
        self.column = Mock()
        self.xview = Mock()
        self.yview = Mock()
        self.configure = Mock()
        self.see = Mock()
        self.__tree = None
        self.__selected = None

    def find_iid(self, iid):
        def find(node):
            if node['iid'] == iid:
                return node
            for child in node['children']:
                found = find(child)
                if found is not None:
                    return found
            return None
        return find(self.__tree)

    def delete(self, iid):
        self.__tree = None

    def insert(self, **kwargs):
        parent = kwargs.get('parent')
        iid = kwargs.get('iid')
        text = kwargs.get('text')
        if parent == '':
            self.__tree = { 'iid': iid, 'text': text, 'children': [] }
            return
        parent_node = self.find_iid(parent)
        parent_node['children'].append({'iid': iid, 'text': text, 'children': []})

    def get_tree_node(self, *indexes):
        node = self.__tree
        for index in indexes:
            node = node['children'][index]
        return node

    def get_tree_node_text(self, *indexes):
        return self.get_tree_node(*indexes)['text']

    def selection_set(self, iid):
        node = self.find_iid(iid)
        self.__selected = [node['iid']]

    def selection_set_by_tree_indexes(self, *indexes):
        node = self.get_tree_node(*indexes)
        self.__selected = [node['iid']]

    def selection(self):
        return self.__selected

    def iid_to_text(self, iid):
        return self.find_iid(iid)['text']


class Listbox(Widget):

    def __init__(self, *args, **kwargs):
        Widget.__init__(self, *args, **kwargs)
        self.xview = Mock()
        self.yview = Mock()
        self.configure = Mock()
        self.size = Mock(return_value=0)
        self.__items = None
        self.__selection = []

    def insert(self, index, *items):
        self.__items = list(items)

    @property
    def items(self):
        return self.__items

    def select(self, *headers):
        selection = []
        for header in headers:
            for index, item in enumerate(self.items):
                if item.startswith(header):
                    selection.append(str(index))
        self.__selection = selection

    def curselection(self):
        return self.__selection
