from __future__ import unicode_literals
import pytest


@pytest.fixture
def mocks(monkeypatch):
    import tkinter_mock
    import devie_mock
    import other_mocks

    class Mocks(object): pass

    m = Mocks()
    m.tkroot = tkinter_mock.install(monkeypatch)
    m.device = devie_mock.install(monkeypatch)
    other_mocks.install(monkeypatch)

    return m
