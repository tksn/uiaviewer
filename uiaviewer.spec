# -*- mode: python -*-

block_cipher = None


def uiautomator_apk(name):
    import os.path
    import uiautomator
    return os.path.join(
        os.path.normpath(uiautomator.__path__[0]),
        os.path.normpath(name))


a = Analysis(['uiaviewer\\main.py'],
             datas=[(uiautomator_apk('libs/*.apk'), 'libs')],
             pathex=['C:\\Users\\tksn\\Development\\uiaviewer'],
             binaries=None,
             hiddenimports=[],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          exclude_binaries=True,
          name='uiaviewer',
          debug=False,
          strip=False,
          upx=True,
          console=True )
coll = COLLECT(exe,
               a.binaries,
               a.zipfiles,
               a.datas,
               strip=False,
               upx=True,
               name='uiaviewer')
