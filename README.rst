uiaviewer
=========
Android UI hierarchy viewer + uiautomator scripting support tool

.. image:: https://ci.appveyor.com/api/projects/status/3vlx83oibgk77e1n?svg=true :target: https://ci.appveyor.com/project/tksn/uiaviewer
.. image:: https://coveralls.io/repos/bitbucket/tksn/uiaviewer/badge.svg?branch=HEAD :target: https://coveralls.io/bitbucket/tksn/uiaviewer?branch=HEAD

Current status
--------------
Just for some experiments

Documents
---------
Not yet
