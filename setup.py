"""Setup module of phoneauto"""

import os
from codecs import open
from setuptools import setup, find_packages

base_dir = os.path.abspath(os.path.dirname(__file__))
with open(os.path.join(base_dir, 'README.rst'), encoding='utf-8') as f:
    long_description = f.read()

setup(
    name='uiaviewer',

    version='0.1.0.dev1',

    description='Android UI hierarchy viewer + uiautomator scripting support tool',
    long_description=long_description,

    url='https://bitbucket.org/tksn/uiaviewer',

    author='Takeshi Naoi',
    author_email='tksnaoi+py@gmail.com',

    license='MIT',

    classifiers=[
        'Development Status :: 3 - Alpha',
        'Intended Audience :: Developers',
        'Topic :: Software Development :: Quality Assurance',

        'License :: OSI Approved :: MIT License',
        'Operating System :: OS Independent',

        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3.5'
    ],

    keywords='android automation uiautomator',

    packages=find_packages(exclude=['test']),

    install_requires=['uiautomator', 'Pillow', 'future'],

    platforms=['Windows'],

    entry_points={
        'console_scripts': [
            'uiaviewer=uiaviewer.uiaview:main'
        ]
    }
)
