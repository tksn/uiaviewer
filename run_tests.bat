cmd.exe "/c activate uiaviewer-py27 && py.test test && deactivate"
cmd.exe "/c activate uiaviewer-py35 && coverage erase && coverage run --source uiaviewer -m py.test test && coverage report && coverage html && deactivate"
cmd.exe "/c activate uiaviewer-py35 && python setup.py flake8 && deactivate"


