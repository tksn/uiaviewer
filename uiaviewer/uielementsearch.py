# -*- coding: utf-8 -*-
"""Android UI element search

:copyright: (c) 2016 by tksn
:license: MIT
"""

from __future__ import unicode_literals
import ast
import itertools
import re


def get_smallest_containing(root, coord):
    """Returns the smallest UI element
    in which bounds specified coordinate is included

    Args:
        root (UiElement): Root element
        coord (tuple): Coordinates
    Returns:
        UiElement: The smallest UI element
    """
    def contains(element):
        """Check coord is inside element.bounds"""
        rect = element.bounds
        if coord[0] < rect['left'] or rect['right'] <= coord[0]:
            return False
        if coord[1] < rect['top'] or rect['bottom'] <= coord[1]:
            return False
        return True

    containing_elements = (
        element for element in root.get_uielement_iterator()
        if contains(element))

    def size(element):
        """Size of element"""
        bounds = element.bounds
        return (
            (bounds['right'] - bounds['left']) +
            (bounds['bottom'] - bounds['top']))

    def smaller(element0, element1):
        """Smaller one of two element"""
        if element0 is None:
            return element1
        if size(element0) > size(element1):
            return element1
        return element0

    smallest = None
    for element in containing_elements:
        smallest = smaller(smallest, element)

    return smallest


def _quote(str_in):
    """Appends and prepends quotation marks to string"""
    return ''.join(('\'', str_in, '\''))


def _bool_str(str_in):
    """Bool value to string"""
    return 'False' if str_in == 'false' else 'True'


_ATTRIBUTE_MAP_HIERARCHYDUMP_TO_UIAUTOMATOR = {
    'checkable': ('checkable', _bool_str),
    'checked': ('checked', _bool_str),
    'class': ('className', _quote),
    'clickable': ('clickable', _bool_str),
    'content-desc': ('description', _quote),
    'enabled': ('enabled', _bool_str),
    'focusable': ('focusable', _bool_str),
    'focused': ('focused', _bool_str),
    'index': ('index', lambda x: x),
    'long-clickable': ('longClickable', _bool_str),
    'package': ('packageName', _quote),
    'resource-id': ('resourceId', _quote),
    'scrollable': ('scrollable', _bool_str),
    'selected': ('selected', _bool_str),
    'text': ('text', _quote)
}


def generate_locator_expression(attributes):
    """Generates xiaocong.uiautomator's locator(selector) expression
    from UI element attributes which is aquired by hierarchy_dump

    Args:
        attributes (iterable): iterable over two-element tuples
            which contains attribute name and attribute value.
            ex) (('class', 'android.Widget.FrameLayout'), ('index', '1'))
    Returns:
        string: string expression of xiaocong.uiautomator's selector
            ex) "class='android.Widget.FrameLayout', index=1"
    """

    expr_list = []
    for attr in attributes:
        conv = _ATTRIBUTE_MAP_HIERARCHYDUMP_TO_UIAUTOMATOR.get(attr[0])
        if conv:
            value_str = conv[1](attr[1])
            expr_list.append('='.join((conv[0], value_str)))
    return ', '.join(expr_list)


def _equals(search_str, attr_str):
    """Equals-operator which is used to find a view"""
    return search_str == attr_str


def _contains(search_str, attr_str):
    """Contains-operator which is used to find a view"""
    return search_str in attr_str


def _startswith(search_str, attr_str):
    """Startswith-operator which is used to find a view"""
    return attr_str.startswith(search_str)


def _matches(search_str, attr_str):
    """Matches-operator which is used to find a view"""
    return bool(re.search(search_str, attr_str))


_ATTRIBUTE_MAP_UIAUTOMATOR_TO_HIERARCHYDUMP = {
    'text': ('text', _equals),
    'textContains': ('text', _contains),
    'textMatches': ('text', _matches),
    'textStartsWith': ('text', _startswith),
    'className': ('class', _equals),
    'classNameMatches': ('class', _matches),
    'description': ('content-desc', _equals),
    'descriptionContains': ('content-desc', _contains),
    'descriptionMatches': ('content-desc', _matches),
    'descriptionStartsWith': ('content-desc', _startswith),
    'checkable': ('checkable', _equals),
    'checked': ('checked', _equals),
    'clickable': ('clickable', _equals),
    'longClickable': ('long-clickable', _equals),
    'scrollable': ('scrollable', _equals),
    'enabled': ('enabled', _equals),
    'focusable': ('focusable', _equals),
    'focused': ('focused', _equals),
    'selected': ('selected', _equals),
    'packageName': ('package', _equals),
    'packageNameMatches': ('package', _matches),
    'resourceId': ('resource-id', _equals),
    'resourceIdMatches': ('resource-id', _matches),
    'index': ('index', _equals)
}


def _get_matchers(criteria):
    """Returns matcher object to find objects which meets criteria """

    def value_to_str(value):
        """value to string conversion used
        in order to stringify lhs value"""
        if isinstance(value, type('')):
            return value
        if isinstance(value, bool):
            return str(value).lower()
        else:
            return str(value)

    def create_matcher_func(key, func, lhsval):
        """Matcher object factory"""

        def matcher(rhs_element):
            """Matcher func"""
            rhsval = rhs_element.get(key, '')
            return func(lhsval, rhsval)
        return matcher

    matchers = []
    for name, value in criteria.items():
        attr_conv = _ATTRIBUTE_MAP_UIAUTOMATOR_TO_HIERARCHYDUMP.get(name)
        if attr_conv is None:
            raise ValueError('Invalid expression: ' + name)
        matchers.append(create_matcher_func(
            key=attr_conv[0], func=attr_conv[1], lhsval=value_to_str(value)))

    return matchers


def quicktest_locator_expression(root, locator_expression):
    """Locator expression check

    Giving tree root and locator, check how many elements are found by the locator in the tree.

    Args:
        root (UiElement): tree root element
        locator_expression (string): locator(selector) expression
            ex) "clickable=True, text='Gmail'"
    Returns:
        int: number of elements found by the locator
    """

    criteria = {}
    for cond in itertools.takewhile(
            lambda x: x.strip(),
            locator_expression.split(',')):
        kv = [x.strip() for x in cond.split('=')]
        if len(kv) != 2:
            raise ValueError('Invalid expression: ' + cond)
        try:
            criteria[kv[0]] = ast.literal_eval(kv[1])
        except Exception:
            raise ValueError('Invalid expression:' + kv[1])

    matchers = _get_matchers(criteria)

    count = 0
    for element in root.get_uielement_iterator():
        match = True
        for matcher in matchers:
            if not matcher(element):
                match = False
                break
        if match:
            count += 1

    return count
