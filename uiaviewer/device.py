# -*- coding: utf-8 -*-
"""Adb/uiautomator device

:copyright: (c) 2016 by tksn
:license: MIT
"""

from __future__ import unicode_literals
import glob
import io
import os
import os.path
import re
import subprocess
import sys
import tempfile
import time
import uuid
from PIL import Image
import uiautomator


class DeviceError(Exception):
    """Device error"""

    def __init__(self, message):
        """Initialization"""
        super(DeviceError, self).__init__(message)


def _run(command):
    """Runs given command as subprocess

    Args:
        command (list): list of command strings
    Returns:
        tuple: (stdout, stderr) of the subprocess
    """
    proc = subprocess.Popen(
        command, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    out_err = proc.communicate()
    if proc.returncode != 0:
        raise DeviceError(
            'Adb command execution failed({0})'.format(' '.join(command)))
    return out_err


class _RawAdbAccess(object):
    """ADB Accessor"""

    def __init__(self, adb_exe):
        """Initialization

        Args:
            adb_exe (str) path to adb executable
        """
        self.__adb_exe = adb_exe

    def get_connected_devices(self):
        """Gives connected devices

        Returns:
            list: list of serial numbers
        """
        out, _ = _run([self.__adb_exe, 'devices'])
        lines = io.StringIO(out.decode('ascii')).readlines()
        devices = []
        for line in lines[1:]:
            columns = line.split()
            if len(columns) == 2 and columns[1] == 'device':
                devices.append(columns[0])
        return devices

    def check_connected(self):
        if len(self.get_connected_devices()) <= 0:
            raise DeviceError('Device not available')

    def wait_for_device(self, timeout=30, polling_interval=2):
        # _run([self.__adb_exe, 'wait-for-device'])
        remaining = timeout
        while remaining > 0:
            if len(self.get_connected_devices()) > 0:
                return
            time.sleep(polling_interval)
            remaining -= polling_interval
        raise DeviceError('Device not available (timeout)')

    def restart_adb_server(self):
        """Restarts adb server"""
        _run([self.__adb_exe, 'kill-server'])
        time.sleep(1)
        _run([self.__adb_exe, 'start-server'])
        time.sleep(1)

    def install(self, apkfile):
        """Installs apk to the device

        Args:
            apkfile (str): path to apk file
        """
        self.check_connected()
        command = [self.__adb_exe, 'install', '-rt', apkfile]
        _run(command)

    def uninstall(self, package):
        """Uninstall app from the device

        Args:
            package (str): package name of the app
        """
        self.check_connected()
        command = [self.__adb_exe, 'uninstall', package]
        _run(command)

    def screencap(self):
        """Take a screen capture from the device

        Returns:
            Image: PIL.Image object
        """
        self.check_connected()
        command = [self.__adb_exe, 'exec-out', 'screencap', '-p']
        out, _ = _run(command)
        screen_image = Image.open(io.BytesIO(out))
        return screen_image

    def hierarchy_dump(self):
        """Take a hierarchy dump from the device

        Returns:
            bytes: hierarchy dump file from the device
        """
        self.check_connected()
        command = [self.__adb_exe, 'shell', 'uiautomator', 'dump']
        out, _ = _run(command)
        m = re.search(b'to:\s+(\S+)', out)
        if not m:
            raise DeviceError('Unable to get dump xml path')
        xml_path = m.group(1).decode('ascii')
        command = ['adb', 'shell', 'cat', xml_path]
        out, _ = _run(command)
        return out

    def get_window_size(self):
        """Gives the window size of the device

        Returns:
            dict: window size parameters in which 'restricted' and 'unrestricted' size are defined.
        """
        self.check_connected()
        command = ['adb', 'shell', 'dumpsys', 'window']
        out, _ = _run(command)

        def extract_size(key):
            """Extracts size (width, size) from dumpsys window output"""
            m = re.search(key + b'=.+\s+([0-9]+)x([0-9]+)', out)
            if not m:
                return None
            return (int(m.group(1)), int(m.group(2)))

        unrestricted_size = extract_size(b'mUnrestrictedScreen')
        if unrestricted_size is None:
            raise DeviceError('Failed to get mUnrestrictedScreen')
        restricted_size = extract_size(b'mRestrictedScreen')
        if restricted_size is None:
            raise DeviceError('Failed to get mRestrictedScreen')

        return {
            'unrestricted': unrestricted_size,
            'restricted': restricted_size
        }


def _null_progress(_):
    """Null progress output function"""
    pass


class Device(object):
    """Android device"""

    def __init__(self, serialno=None, progress=_null_progress):
        """Initialization

        Args:
            serialno (str): Serial number of the android device
            progress (func): optional function to report progress
        """
        self.__auto_device = uiautomator.Device(serial=serialno)
        self.__rawadb = _RawAdbAccess(self.__auto_device.server.adb.adb())
        self.__rawadb.check_connected()
        self.__ensure_uiautomator_server_inactive(progress)
        self.__server_activated = False
        self.__method = {
            'screencap': self.__rawadb.screencap,
            'hierarchy_dump': self.__rawadb.hierarchy_dump
        }

    def __screencap(self):
        """Gets screen capture using uiautomator"""
        file_path = os.path.join(
            tempfile.gettempdir(), 'tmp_{0}.png'.format(uuid.uuid4()))
        if not self.__auto_device.screenshot(file_path):
            raise DeviceError('Failed to get screenshot via uiautomator')
        return Image.open(file_path)

    def __hierarchy_dump(self):
        """Gets UI hierarchy dump using uiautomator"""
        return self.__auto_device.dump(compressed=False, pretty=False).encode('utf-8')

    def __ensure_uiautomator_server_inactive(self, progress):
        """Ensure uiautomator server is inactive

        Since residual uiautomator process in the device can cause some issue,
        it is better to remove it first. In order to do that,
        This function stops uiautomator server and then
        uninstalls uiautomator app from the device.
        """
        progress('Stopping residual uiautomator server')
        self.__auto_device.server.stop()
        progress('Restarting adb server')
        progress('   Please tap "OK" on "Allow usb debugging" popup on the device')
        self.__rawadb.restart_adb_server()
        self.__rawadb.wait_for_device()
        progress('Uninstalling residual uiautomator app')
        self.__rawadb.uninstall('com.github.uiautomator')

    def activate_uiautomator_server(self):
        """Activates uiautomator server on the device"""
        if getattr(sys, 'frozen', False):
            # When this app is executed as PyInstaller bundle, uiautomator's apk installation
            # function doesn't work correctly  because of __file__ used in uiautomator.
            # As a workaround, we install required apk here beforehand.
            apkfiles = glob.glob(os.path.join(sys._MEIPASS, 'libs/*.apk'))
            for apkfile in apkfiles:
                self.__rawadb.install(os.path.join(sys._MEIPASS, apkfile))
        self.__auto_device.server.start()
        self.__server_activated = True
        self.__method = {
            'screencap': self.__screencap,
            'hierarchy_dump': self.__hierarchy_dump
        }

    def deactivate_uiautomator_server(self):
        """Deactivate uiautomator server on the device"""
        self.__auto_device.server.stop()
        self.__server_activated = False
        self.__method = {
            'screencap': self.__rawadb.screencap,
            'hierarchy_dump': self.__rawadb.hierarchy_dump
        }

    @property
    def uiautomator_server_activated(self):
        """Gives if uiautomator server has been activated"""
        return self.__server_activated

    def screencap(self):
        """Take screen capture from the device"""
        return self.__method['screencap']()

    def hierarchy_dump(self):
        """Take UI hierarchy dump fromt the device"""
        return self.__method['hierarchy_dump']()

    def get_window_size(self):
        """Gives device screen size"""
        return self.__rawadb.get_window_size()

    @property
    def uiautomator_device(self):
        """Gives internal uiautomator object"""
        if self.uiautomator_server_activated:
            return self.__auto_device
        return None
