# -*- coding: utf-8 -*-
"""uiaviewer application main

:copyright: (c) 2016 by tksn
:license: MIT
"""

from __future__ import unicode_literals
import argparse
import logging
import sys
from uiaviewer.control import Control
from uiaviewer.gui.mainwindow import MainWindow


def size_parse(size_str):
    """Parse size string"""
    return tuple(int(t) for t in size_str.split('x'))[:2]


def parse_args(argv):
    """Parse command line arguments"""
    parser = argparse.ArgumentParser(
        description='Android UI hierarchy viewer with some tools for uiautomator scripting')
    parser.add_argument(
        '-w', '--screenshot_window_size', dest='screen_size', default='480x800',
        type=size_parse,
        help='size (WIDTHxHEIGHT) of the screenshot window (default: 480x800)')
    args = parser.parse_args(argv)
    return args


def run_viewer(argv):
    """Run uiaviewer application"""
    args = parse_args(argv)
    control = Control(
        settings={
            'screen_size': args.screen_size
        }
    )
    gui = MainWindow(control)
    gui.run()


def main():
    """Entry point of uiaviewer"""
    run_viewer(sys.argv[1:])


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)
    main()
