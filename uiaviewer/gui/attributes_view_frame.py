from __future__ import unicode_literals
import tkinter
from tkinter import ttk
from uiaviewer.gui.common import GuiFragment


class AttributesViewFrame(GuiFragment):

    def __init__(self, control, listbox_size, master, name):
        self.__control = control
        self.__frame = ttk.Frame(master, name=name)
        super(AttributesViewFrame, self).__init__(self.__frame)
        self.__control.add_listener('node_selection_changed', self.populate)
        self.__current = None
        self.__attrs = None

        innerframe = ttk.Frame(
            self.__frame, width=listbox_size[0], height=listbox_size[1])
        innerframe.pack_propagate(0)

        self.__listbox = tkinter.Listbox(
            innerframe, selectmode=tkinter.MULTIPLE, exportselection=0, name='attrlistbox')
        self.__listbox.pack(side=tkinter.LEFT, fill=tkinter.BOTH, expand=True)
        innerframe.grid(row=0, column=0, sticky=tkinter.NW)

        xsb = ttk.Scrollbar(
            self.__frame, orient=tkinter.HORIZONTAL, command=self.__listbox.xview)
        ysb = ttk.Scrollbar(
            self.__frame, orient=tkinter.VERTICAL, command=self.__listbox.yview)
        self.__listbox.configure(xscrollcommand=xsb.set, yscrollcommand=ysb.set)
        xsb.grid(row=1, column=0, sticky=tkinter.EW)
        ysb.grid(row=0, column=1, sticky=tkinter.NS)

        self.poll()

    def populate(self, elem):
        if self.__listbox.size() > 0:
            self.__listbox.delete(0, self.__listbox.size() - 1)
        self.__current = None
        if elem is None:
            return
        self.__attrs = list(sorted(elem.attrib.items(), key=lambda kv: kv[0]))
        disp_attrs = ['{0}: {1}'.format(*attr) for attr in self.__attrs]
        self.__listbox.insert(tkinter.END, *disp_attrs)

    def poll(self):
        now = self.__listbox.curselection()
        if now != self.__current:
            self.__current = now
            self.on_selectionchanged(now)
        self.__listbox.after(250, self.poll)

    def on_selectionchanged(self, selection):
        indices = (int(index_str) for index_str in selection)
        items = [self.__attrs[index] for index in indices]
        self.__control.signal('attribute_selection_changed', items)
