from __future__ import unicode_literals
import tkinter
from tkinter import ttk
import uiaviewer.gui.common
from uiaviewer.gui.startwindow import StartWindow
from uiaviewer.gui.stillscreen import StillScreen
from uiaviewer.gui.hierarchy_view_frame import HierarchyViewFrame
from uiaviewer.gui.attributes_view_frame import AttributesViewFrame
from uiaviewer.gui.locator_check_frame import LocatorCheckFrame
from uiaviewer.gui.immediate_evaluation_frame import ImmediateEvaluationFrame


class MainWindow(object):

    def __init__(self, control):
        self.__control = control
        self.__root = None

    def run(self):
        self.__root = self.__build_phase1()

        startwin = StartWindow(self.__control, self.__root)
        if startwin.wait_complete():
            self.__root.after(1, self.__build_phase2)
            self.__root.mainloop()

    def __build_phase1(self):
        root = tkinter.Tk()
        root.title('uiaviewer')

        uiaviewer.gui.common.init()

        root.update()
        return root

    def __build_phase2(self):
        mainframe = ttk.Frame(self.__root, name='mainframe')
        mainframe.grid(row=0, column=0, sticky=tkinter.NSEW)

        screen = StillScreen(self.__control, mainframe, name='stillscreen')
        screen.grid(row=0, column=0, sticky=tkinter.NSEW)

        rightpane = ttk.Frame(mainframe, name='rightpane')
        rightpane.grid(row=0, column=1, sticky=tkinter.NSEW)

        topbar = ttk.Frame(rightpane, name='topbar')
        topbar.pack(fill=tkinter.X)

        self.__update_button = ttk.Button(
            topbar, text='Update', command=self.on_update_clicked, name='update')
        self.__update_button.pack(side=tkinter.LEFT)

        self.__connect_button = ttk.Button(
            topbar, text='Activate uiautomator', command=self.on_connect_clicked,
            name='activateuiautomator')
        self.__connect_button.pack(side=tkinter.RIGHT)

        hierarchy_label = ttk.Label(
            rightpane, text='UI Hierarchy', style='Header1.TLabel')
        hierarchy_label.pack(anchor=tkinter.W)

        hierarchy = HierarchyViewFrame(
            self.__control, (640, 200), rightpane, name='hviewframe')
        hierarchy.pack()

        rightmidpanel_label = ttk.Label(
            rightpane, text='Attributes', style='Header1.TLabel')
        rightmidpanel_label.pack(anchor=tkinter.W)

        rightmidpanel = ttk.Frame(rightpane, name='rightmidpanel')
        rightmidpanel.pack(fill=tkinter.X)

        attributes = AttributesViewFrame(
            self.__control, (400, 270), rightmidpanel, name='attrviewframe')
        attributes.pack(side=tkinter.LEFT)

        locator_check = LocatorCheckFrame(
            self.__control, (220, 100), rightmidpanel, name='locatorcheckframe')
        locator_check.pack(side=tkinter.LEFT, anchor=tkinter.NW, padx=5)

        eval_frame_label = ttk.Label(
            rightpane, text='Evaluation', style='Header1.TLabel')
        eval_frame_label.pack(anchor=tkinter.W)

        self.__eval_frame = ImmediateEvaluationFrame(
            self.__control, 640, rightpane, name='evalframe')
        self.__eval_frame.pack(anchor=tkinter.W)

        self.__control.signal('initialize')
        self.__root.update()

    def on_update_clicked(self):
        with uiaviewer.gui.common.display_wait(self.__root):
            self.__control.signal('update')

    def on_connect_clicked(self):
        if self.__control.uiautomator_activated:
            try:
                with uiaviewer.gui.common.display_wait(self.__root):
                    self.__control.deactivate_uiautomator()
                self.__connect_button.config(text='Activate uiautomator')
                self.__eval_frame.disable()
            except Exception:
                uiaviewer.gui.common.popup(self.__root, 'Failed to deactivate')
        else:
            try:
                with uiaviewer.gui.common.display_wait(self.__root):
                    self.__control.activate_uiautomator()
                self.__connect_button.config(text='Deactivate uiautomator')
                self.__eval_frame.enable()
            except Exception:
                uiaviewer.gui.common.popup(self.__root, 'Failed to activate')
