from __future__ import unicode_literals
import tkinter
from PIL import ImageTk


class ScreenDraw(object):

    def __init__(self, canvas, control):
        self.__canvas = canvas
        self.__control = control
        self.__cur_image = {}
        self.__rect_id = None
        self.__control.add_listener(
            'node_selection_changed', self.draw_uielement)

    def set_image(self, image):
        if self.__cur_image:
            self.__canvas.delete(self.__cur_image['id'])
        pimage = ImageTk.PhotoImage(image)
        image_id = self.__canvas.create_image(
            0, 0, anchor=tkinter.NW, image=pimage)
        self.__cur_image = {'id': image_id, 'image': pimage}

    def draw_uielement(self, element, color='red'):
        if self.__rect_id is not None:
            self.__canvas.delete(self.__rect_id)
            self.__rect_id = None
        if element is None:
            return

        bounds = element.bounds
        if bounds is None:
            return

        geom = self.__control.geometry
        xy0 = geom.scale(geom.clip((bounds['left'], bounds['top'])))
        xy1 = geom.scale(geom.clip((bounds['right'], bounds['bottom'])))
        self.__rect_id = self.__canvas.create_rectangle(
            xy0[0], xy0[1], xy1[0], xy1[1],
            outline=color, width=2, tag='')
