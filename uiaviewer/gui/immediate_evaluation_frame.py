from __future__ import unicode_literals
import tkinter
from tkinter import ttk
from uiaviewer.gui.common import GuiFragment


class ImmediateEvaluationFrame(GuiFragment):

    def __init__(self, control, width, master, name):
        self.__control = control
        self.__frame = ttk.Frame(master, name=name)
        super(ImmediateEvaluationFrame, self).__init__(self.__frame)

        desc_frame = ttk.Frame(self.__frame)
        desc = ttk.Label(
            desc_frame,
            text=''.join(('Enter an expression such as ',
                          'd(text=\'Gmail\').click.wait(). ',
                          'Device can be referred as "d"')))
        desc.pack(side=tkinter.LEFT)
        desc_frame.pack(fill=tkinter.X, expand=True)

        entry_frame = ttk.Frame(self.__frame, width=width, height=30)
        entry_frame.pack_propagate(0)
        self.entry = ttk.Entry(entry_frame)
        self.entry.pack(side=tkinter.LEFT, fill=tkinter.X, expand=True)
        self.entry.bind('<Return>', self.on_eval_button)
        self.clear_button = ttk.Button(entry_frame, text='Clear', command=self.on_clear_button)
        self.clear_button.pack(side=tkinter.LEFT)
        self.eval_button = ttk.Button(entry_frame, text='Eval', command=self.on_eval_button)
        self.eval_button.pack(side=tkinter.LEFT)
        entry_frame.pack(fill=tkinter.X, expand=True)

        output_frame = ttk.Frame(self.__frame)
        eval_result_frame = ttk.Labelframe(output_frame, text='Result')
        self.eval_result = ttk.Label(eval_result_frame, text='')
        self.eval_result.pack(side=tkinter.LEFT)
        eval_result_frame.grid(row=0, column=0, sticky=tkinter.NSEW)
        eval_error_frame = ttk.Labelframe(output_frame, text='Error')
        self.eval_error = ttk.Label(eval_error_frame, text='')
        self.eval_error.pack(side=tkinter.LEFT)
        eval_error_frame.grid(row=0, column=1, sticky=tkinter.NSEW)
        output_frame.columnconfigure(0, weight=1)
        output_frame.columnconfigure(1, weight=1)
        output_frame.pack(fill=tkinter.X, expand=True)

        self.disable()

    def enable(self):
        self.entry.config(state=tkinter.ACTIVE)
        self.eval_button.config(state=tkinter.ACTIVE)
        self.clear_button.config(state=tkinter.ACTIVE)
        self.entry.delete(0, tkinter.END)

    def disable(self):
        self.entry.delete(0, tkinter.END)
        self.entry.insert(0, 'Activate uiautomator to enable evaluation')
        self.entry.config(state=tkinter.DISABLED)
        self.eval_button.config(state=tkinter.DISABLED)
        self.clear_button.config(state=tkinter.DISABLED)

    def on_eval_button(self, event=None):
        text = self.entry.get()
        eval_globals = {}
        eval_locals = {'d': self.__control.uiautomator_device}
        try:
            result = eval(text, eval_globals, eval_locals)
            self.eval_result.config(text=str(result))
            self.eval_error.config(text='')
        except Exception as e:
            self.eval_result.config(text='')
            self.eval_error.config(text=str(e))

    def on_clear_button(self, event=None):
        self.entry.delete(0, tkinter.END)
