from __future__ import unicode_literals
from queue import Queue
import threading
import tkinter


def _control_open(control, progress_queue):

    def progress(text):
        progress_queue.put(text)

    if control.open(progress):
        progress_queue.put('[OK]')
    else:
        progress_queue.put('[ERROR]')


class StartWindow(object):

    def __init__(self, control, master):
        self.__control = control
        self.__window = tkinter.Toplevel(master)
        super(StartWindow, self).__init__()

        self.__textwidget = tkinter.Text(self.__window)
        self.__textwidget.pack()

        self.__progress_queue = Queue()
        self.__thread = threading.Thread(
            target=_control_open, args=(control, self.__progress_queue))
        self.__thread.start()
        self.__success = False
        self.poll()

    def poll(self):
        while not self.__progress_queue.empty():
            text = self.__progress_queue.get_nowait()
            if text == '[OK]':
                self.__window.destroy()
                self.__success = True
                return
            if text == '[ERROR]':
                self.__window.after(5000, self.error_quit)
            self.__textwidget.insert(tkinter.END, text + '\n')
            self.__textwidget.see(tkinter.END)
        self.__window.after(500, self.poll)

    def error_quit(self):
        self.__window.destroy()

    def wait_complete(self):
        self.__window.master.wait_window(window=self.__window)
        return self.__success
