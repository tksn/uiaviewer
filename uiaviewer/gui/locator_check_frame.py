from __future__ import unicode_literals
import tkinter
from tkinter import ttk
import uiaviewer.uielementsearch
from uiaviewer.gui.common import GuiFragment


class LocatorCheckFrame(GuiFragment):

    def __init__(self, control, expr_box_size, master, name):
        self.__control = control
        self.__frame = ttk.Frame(master, name=name)
        super(LocatorCheckFrame, self).__init__(self.__frame)
        self.__control.add_listener(
            'attribute_selection_changed', self.update_locator_expression)
        self.__current = None

        loc_expr_label = ttk.Label(
            self.__frame, text='Locator expression', style='Header2.TLabel')
        loc_expr_label.grid(row=0, column=0, sticky=tkinter.NW)

        innerframe = ttk.Frame(
            self.__frame, width=expr_box_size[0], height=expr_box_size[1])
        innerframe.pack_propagate(0)
        self.__loc_expr = tkinter.Text(innerframe, exportselection=0, name='locexprtext')
        self.__loc_expr.pack(side=tkinter.LEFT, fill=tkinter.BOTH, expand=True)
        innerframe.grid(row=1, column=0)

        self.__match_count = ttk.Label(
            self.__frame, text='', wraplength=expr_box_size[0], name='matchcount')
        self.__match_count.grid(row=2, column=0, sticky=tkinter.NW)

        copy_btn = ttk.Button(
            self.__frame, text='Copy', command=self.copy, name='locexprcopy')
        copy_btn.grid(row=3, column=0, sticky=tkinter.NE)

        self.poll()

    def update_locator_expression(self, attrs):
        self.__loc_expr.delete(1.0, tkinter.END)
        expr = uiaviewer.uielementsearch.generate_locator_expression(attrs)
        self.__loc_expr.insert(tkinter.END, expr)

    def poll(self):
        now = self.__loc_expr.get(1.0, tkinter.END)
        if now != self.__current:
            self.__current = now
            self.on_textchanged(now)
        self.__loc_expr.after(250, self.poll)

    def on_textchanged(self, text):

        if not text.strip():
            self.__match_count.config(text='')
            return

        tree_root = self.__control.get_hierarchy_snapshot()
        try:
            count = uiaviewer.uielementsearch.quicktest_locator_expression(tree_root, text)
            self.__match_count.config(text='Matches to {0} element(s).'.format(count))
        except ValueError as err:
            self.__match_count.config(text=str(err))

    def copy(self):
        self.__loc_expr.clipboard_clear()
        self.__loc_expr.clipboard_append(self.__current)
