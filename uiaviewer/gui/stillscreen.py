from __future__ import unicode_literals
import tkinter
from tkinter import ttk
from uiaviewer.gui.common import GuiFragment
from uiaviewer.gui.screendraw import ScreenDraw


class StillScreen(GuiFragment):

    def __init__(self, control, master, name):
        self.__control = control
        self.__frame = ttk.Frame(master, name=name)
        super(StillScreen, self).__init__(self.__frame)

        self.__size = self.__control.geometry.scaled_screen_size
        self.__original_size = None
        canvas = tkinter.Canvas(
            self.__frame, width=self.__size[0], height=self.__size[1], name='canvas')
        canvas.pack()
        self.__draw = ScreenDraw(canvas, self.__control)
        self.__refresh(initial=True)
        canvas.bind('<Button-1>', self.__on_click)

        self.__control.add_listener('update', self.__refresh)

    def __refresh(self, initial=False):
        if initial:
            image = self.__control.get_screenshot()
        else:
            image = self.__control.take_screenshot()
        self.__original_size = image.size
        scaled_image = image.resize(self.__size)
        self.__draw.set_image(scaled_image)

    def __on_click(self, event):
        descale_xy = self.__control.geometry.descale((event.x, event.y))
        self.__control.signal('screen_clicked', descale_xy)
