from __future__ import unicode_literals
import tkinter
from tkinter import ttk
import uiaviewer.uielementsearch
from uiaviewer.gui.common import GuiFragment


class HierarchyViewFrame(GuiFragment):

    def __init__(self, control, treeview_size, master, name):
        self.__control = control
        self.__frame = ttk.Frame(master, name=name)
        super(HierarchyViewFrame, self).__init__(self.__frame)
        self.__id_to_node = {}
        self.__root_id = None

        # TreeView
        innerframe = ttk.Frame(
            self.__frame, width=treeview_size[0], height=treeview_size[1])
        innerframe.pack_propagate(0)
        self.__treeview = ttk.Treeview(innerframe, show='tree', name='hviewtreeview')
        self.__treeview.column('#0', width=1920)
        self.__treeview.pack()
        innerframe.grid(row=0, column=0, sticky=tkinter.NW)

        # Scrollbar
        xsb = ttk.Scrollbar(
            self.__frame, orient=tkinter.HORIZONTAL, command=self.__treeview.xview)
        ysb = ttk.Scrollbar(
            self.__frame, orient=tkinter.VERTICAL, command=self.__treeview.yview)
        self.__treeview.configure(xscrollcommand=xsb.set, yscrollcommand=ysb.set)
        xsb.grid(row=1, column=0, sticky=tkinter.EW)
        ysb.grid(row=0, column=1, sticky=tkinter.NS)

        # Event binding
        self.__control.add_listener('initialize', self.populate)
        self.__control.add_listener('update', self.populate)
        self.__control.add_listener('screen_clicked', self.on_screen_clicked)
        self.__treeview.bind('<<TreeviewSelect>>', self.on_selectionchanged)

    def populate(self):
        tv = self.__treeview
        self.__id_to_node = {}
        if self.__root_id is not None:
            tv.delete(self.__root_id)
            self.__root_id = None

        tree_root = self.__control.take_hierarchy_snapshot()
        if tree_root is None:
            return

        def traversal_insert(parent_id, elem):
            iid = str(id(elem))
            tv.insert(
                parent=parent_id, index=tkinter.END,
                iid=iid, text=str(elem), open=True)
            self.__id_to_node[iid] = elem
            for subelem in elem:
                traversal_insert(iid, subelem)
            return iid

        self.__root_id = traversal_insert('', tree_root)
        tv.see(str(id(tree_root)))
        self.__control.signal('node_selection_changed', None)

    def on_selectionchanged(self, event):
        for iid in self.__treeview.selection():
            node = self.__id_to_node.get(iid)
            if node is not None:
                self.__control.signal('node_selection_changed', node)

    def on_screen_clicked(self, coord):
        tree_root = self.__control.get_hierarchy_snapshot()
        element = uiaviewer.uielementsearch.get_smallest_containing(tree_root, coord)
        if element is not None:
            self.__treeview.selection_set(str(id(element)))
            self.__treeview.see(str(id(element)))
