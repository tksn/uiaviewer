from __future__ import unicode_literals
import contextlib
import tkinter
from tkinter import ttk


def init():
    header_style1 = ttk.Style()
    header_style1.configure('Header1.TLabel', font=('Sans', '10', 'bold'))

    header_style2 = ttk.Style()
    header_style2.configure(
        'Header2.TLabel', foreground='darkblue')


@contextlib.contextmanager
def display_wait(widget):
    """Displays wait icon while context is alive"""
    toplevel = widget.winfo_toplevel()
    toplevel.config(cursor='wait')
    toplevel.update()
    yield
    toplevel.config(cursor='')


def popup(master, message):
    window = tkinter.Toplevel(master)
    label = ttk.Label(window, text=message)
    label.pack()
    button = ttk.Button(window, text='OK')
    button.pack()
    master.wait_window(window=window)


class GuiFragment(object):

    def __init__(self, base_tk_widget):
        self.__widget = base_tk_widget

    def pack(self, **kwargs):
        self.__widget.pack(**kwargs)

    def grid(self, **kwargs):
        self.__widget.grid(**kwargs)
