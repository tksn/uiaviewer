# -*- coding: utf-8 -*-
"""Android UI hierarchy

:copyright: (c) 2016 by tksn
:license: MIT
"""

from __future__ import unicode_literals
import io
import re
import xml.etree.ElementTree as ET
from future.utils import python_2_unicode_compatible


@python_2_unicode_compatible
class UiElement(ET.Element):
    """UI Element class which represents a node in Android UI hierarchy
    """

    def __init__(self, tag, attrib):
        """Initializes UiElement object

        Args:
            tag (str) xml tag
            attrib (dict) attributes
        """
        super(UiElement, self).__init__(tag, attrib)

    def __str__(self):
        """String reprisentation

        UiElement can be converted to string which format is similar to
        what user can see in hierarchy view of uiautomatorviewer.
        """
        disp_attr = []

        disp_attr.append('({0})'.format(self.get('index', '')))

        classname = self.get('class', '')
        for known_pkg in ('android.widget.', 'android.view.'):
            classname = classname.replace(known_pkg, '')
        if classname:
            disp_attr.append(classname)

        text = self.get('text')
        if text:
            disp_attr.append(''.join(('"', text, '"')))

        desc = self.get('content-desc')
        if desc:
            disp_attr.append(''.join(('{', desc, '}')))

        bounds = self.get('bounds')
        if bounds:
            disp_attr.append(bounds)

        return ' '.join(disp_attr)

    @property
    def bounds(self):
        """Bounds of the UI Element

        Returns:
            dict: None if the element is root.
                Otherwise, dictionary which contains 'top', 'left', 'bottom' and 'right'.
                Each value is integer.
        """
        bounds_str = self.get('bounds')
        if not bounds_str:
            return None
        dig = r'[+-]?\d+'
        pat = r'\[({dig}),({dig})\]\[({dig}),({dig})\]'.format(dig=dig)
        match_result = re.match(pat, bounds_str)
        if not match_result or len(match_result.groups()) != 4:
            raise ValueError('Hierarchy dump contains invalid bounds value')
        return dict(zip(
            ('left', 'top', 'right', 'bottom'),
            (int(v) for v in match_result.groups())))

    def get_uielement_iterator(self):
        """Return iterator which iterates over all the UiElements
        in the sub-tree of this element.
        """
        return super(UiElement, self).iter('node')


class UiHierarchy(object):
    """UI Hierarchy utility class
    """

    @staticmethod
    def frombytes(text):
        """Parses XML string and returns root UiElement of UI hierarchy

        Args:
            text (byte): XML string
        Returns:
            UiElement: Tree root of Ui hierarchy
        """
        strin = io.BytesIO(text)
        parser = ET.XMLParser(target=ET.TreeBuilder(element_factory=UiElement))
        tree = ET.parse(strin, parser)
        return tree.getroot()
