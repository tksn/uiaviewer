# -*- coding: utf-8 -*-
"""uiaviewer controller

:copyright: (c) 2016 by tksn
:license: MIT
"""

from __future__ import unicode_literals
from __future__ import division
from uiaviewer.device import Device
from uiaviewer.uihierarchy import UiHierarchy


class ScreenGeometry(object):
    """Screen geometry

    Does coordinates conversion between scaled screen(=screen presented on PC) and
    descaled screen (=screen presented on the device).
    """

    def __init__(self, device, screen_size):
        """Inilialization

        Args:
            device (Device): device object
            screen_size (tuple): scaled screen size (width, height)
        """
        self.__window_size = device.get_window_size()
        device_screen_size = self.__window_size['unrestricted']
        self.scale_factor = (
            screen_size[0] / device_screen_size[0],
            screen_size[1] / device_screen_size[1])
        self.__scaled_screen_size = screen_size

    @property
    def scaled_screen_size(self):
        """Scaled screen size (width, height)"""
        return self.__scaled_screen_size

    @property
    def device_restricted_screen_size(self):
        """Restricted descaled screen size (width, height)

        Returns:
            tuple: size (width, height) of the device's restricted screen
                (=screen area excluding navigation area)
        """
        return self.__window_size['restricted']

    @property
    def device_unrestricted_screen_size(self):
        """Unestricted descaled screen size (width, height)"""
        return self.__window_size['unrestricted']

    def scale(self, coord):
        """Converts coordinates on descaled screen to coordinates on scaled screen

        Args:
            coord (tuple): coordinates (x, y) on descaled (=device) screen
        Returns:
            tuple: coordinates (x, y) on scaled (=this PC app) screen
        """
        return (
            coord[0] * self.scale_factor[0],
            coord[1] * self.scale_factor[1])

    def descale(self, coord):
        """Converts coordinates on scaled screen to coordinates on descaled screen

        Args:
            coord (tuple): coordinates (x, y) on scaled (=this PC app) screen
        Returns:
            tuple: coordinates (x, y) on descaled (=device) screen
        """
        return (
            coord[0] / self.scale_factor[0],
            coord[1] / self.scale_factor[1])

    def clip(self, descaled_coord):
        """Clips coordinates into device's restricted screen size

        Args:
            descaled_coord: coordinates (x, y) on the device screen
        Returns:
            tuple: clipped coordinates (x, y)
        """
        def _clip(val, maxval):
            """Clip value into [0, maxval]"""
            return min(max(0, val), maxval)
        screen_size = self.device_restricted_screen_size
        clipped_x = _clip(descaled_coord[0], screen_size[0])
        clipped_y = _clip(descaled_coord[1], screen_size[1])
        return (clipped_x, clipped_y)


class Control(object):
    """App controller object"""

    def __init__(self, settings=None):
        """Initialization"""
        self.__listener = {}
        self.__settings = settings or {
            'screen_size': (480, 800)
        }
        self.__device = None
        self.__geometry = None
        self.__hierarchy_snapshot = None
        self.__screenshot = None

    def open(self, progress):
        """Opens device

        This process includes,
        cleaning up residual uiautomator processes,
        and aquiring first screenshot and UI hierarchy snapshot.
        It may take a while.

        Args:
            progress (function): text printer to report progress
        Returns:
            bool: True if succeed
        """
        try:
            progress('Device initialization START')
            self.__device = Device(progress=progress)
            self.__geometry = ScreenGeometry(
                self.__device, self.__settings['screen_size'])
            progress('Device initialization END')

            progress('Acquiring screen capture')
            self.take_screenshot()
            progress('Acquiring UI hierarchy dump')
            self.take_hierarchy_snapshot()
        except Exception as exp:
            progress(str(exp))
            return False
        return True

    def add_listener(self, signal_name, listener):
        """Adds listener function object to a signal

        Args:
            signal_name (str): name of the signal
            listener (callable): listener object
        """
        self.__listener.setdefault(signal_name, []).append(listener)

    def signal(self, name, *args, **kwargs):
        """Calls all function objects which are connected to a signal

        Args:
            name (str): name of the signal
            args (list): arguments to listener
            kwargs (dict): keyword arguments to listener
        """
        for listener in self.__listener.get(name, []):
            listener(*args, **kwargs)

    def activate_uiautomator(self):
        """Activates uiautomator

        Installs uiautomator, and starts uiautomator server.
        """
        if self.uiautomator_activated:
            return
        self.__device.activate_uiautomator_server()

    def deactivate_uiautomator(self):
        """Dectivates uiautomator

        Stops uiautomator server.
        """
        if not self.uiautomator_activated:
            return
        self.__device.deactivate_uiautomator_server()

    @property
    def uiautomator_activated(self):
        """If uiautomator has been activated or not"""
        return self.__device.uiautomator_server_activated

    @property
    def uiautomator_device(self):
        """Internal uiautomator object"""
        return self.__device.uiautomator_device

    @property
    def geometry(self):
        """Geometry object"""
        return self.__geometry

    def take_screenshot(self):
        """Takes screenshot from the device

        Returns:
            object: taken screenshot as PIL.Image object
        """
        self.__screenshot = self.__device.screencap()
        return self.__screenshot

    def get_screenshot(self):
        """Gives previously taken screenshot from the device

        Returns:
            object: taken screenshot as PIL.Image object
        """
        return self.__screenshot

    def take_hierarchy_snapshot(self):
        """Takes UI hierarchy tree from the device

        Returns:
            object: UI hierarchy tree
        """
        self.__hierarchy_snapshot = UiHierarchy.frombytes(
            self.__device.hierarchy_dump())
        return self.__hierarchy_snapshot

    def get_hierarchy_snapshot(self):
        """Gives previously taken UI hierarchy tree

        Returns:
            object: UI hierarchy tree
        """
        return self.__hierarchy_snapshot
